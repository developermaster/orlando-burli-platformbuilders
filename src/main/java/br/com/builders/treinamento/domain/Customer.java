package br.com.builders.treinamento.domain;

import java.io.Serializable;

import javax.persistence.Id;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customers")
public class Customer implements Serializable {

	private static final long serialVersionUID = 5309164256365190382L;

	@Id
	private String id;

	@NotEmpty(message = "{customer.name.mandatory}")
	private String name;

	private String crmId;

	private String baseUrl;

	private String login;

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCrmId() {
		return this.crmId;
	}

	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}

	public String getBaseUrl() {
		return this.baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
}