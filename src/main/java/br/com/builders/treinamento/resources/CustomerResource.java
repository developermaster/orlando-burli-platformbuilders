package br.com.builders.treinamento.resources;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.builders.treinamento.dto.CustomerDto;
import br.com.builders.treinamento.exception.customers.CustomerException;
import br.com.builders.treinamento.exception.customers.CustomerNotFoundException;
import br.com.builders.treinamento.service.CustomerService;

@RestController
@RequestMapping("/customers")
public class CustomerResource {

	private final CustomerService customerService;

	@Autowired
	public CustomerResource(final CustomerService customerService) {
		this.customerService = customerService;
	}

	@PostMapping
	public ResponseEntity<Void> insert(@RequestBody final CustomerDto customer) throws CustomerException {
		final CustomerDto customerSaved = this.customerService.insert(customer);

		final URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{customerId}")
				.buildAndExpand(customerSaved.getId()).toUri();

		return ResponseEntity.created(location).build();
	}

	@PutMapping("/{customerId}")
	public ResponseEntity<Void> update(@PathVariable("customerId") String customerId,
			@RequestBody final CustomerDto customer) throws CustomerException {

		this.customerService.update(customerId, customer);

		return ResponseEntity.ok().build();
	}

	@PatchMapping("/{customerId}")
	public ResponseEntity<Void> patch(@PathVariable("customerId") String customerId,
			@RequestBody final CustomerDto customer) throws CustomerException {

		this.customerService.patch(customerId, customer);

		return ResponseEntity.ok().build();
	}

	@DeleteMapping("/{customerId}")
	public ResponseEntity<Void> delete(@PathVariable("customerId") String customerId) throws CustomerException {

		this.customerService.delete(customerId);

		return ResponseEntity.ok().build();
	}

	@GetMapping("/{customerId}")
	public CustomerDto get(@PathVariable("customerId") String customerId) throws CustomerNotFoundException {
		return this.customerService.get(customerId);
	}

	@GetMapping
	public List<CustomerDto> listAll() {
		return this.customerService.listAll();
	}
}
