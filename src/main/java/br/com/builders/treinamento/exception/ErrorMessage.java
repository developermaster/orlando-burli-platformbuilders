/*
* Copyright 2018 Builders
*************************************************************
*Nome     : ErrorMessage.java
*Autor    : Builders
*Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.exception;

import java.io.Serializable;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ErrorMessage implements Serializable {

	private static final long serialVersionUID = -40089374255050241L;

	private String code;

	private String message;

	public ErrorMessage(final String code, final String msg) {
		this.setCode(code);
		this.setMessage(msg);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(final String code) {
		this.code = StringUtils.trimAllWhitespace(code);
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(final String message) {
		this.message = StringUtils.trimAllWhitespace(message);
	}
}
