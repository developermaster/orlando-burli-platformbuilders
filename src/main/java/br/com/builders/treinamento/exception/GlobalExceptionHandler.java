/*
* Copyright 2018 Builders
*************************************************************
*Nome     : GlobalExceptionHandler.java
*Autor    : Builders
*Data     : Thu Mar 08 2018 00:02:30 GMT-0300 (-03)
*Empresa  : Platform Builders
*************************************************************
*/
package br.com.builders.treinamento.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.builders.treinamento.exception.customers.CustomerException;

@ControllerAdvice
public class GlobalExceptionHandler {

	/**
	 * Intercepts all customer exceptions.
	 *
	 * @param customerException
	 *            Exception catched.
	 * @return Response to the server.
	 */
	@ExceptionHandler(CustomerException.class)
	protected ResponseEntity<ErrorMessage> processCustomerException(final CustomerException customerException) {

		final ResponseStatus responseStatus = customerException.getClass().getAnnotation(ResponseStatus.class);

		final HttpStatus httpStatus = responseStatus != null ? responseStatus.value() : HttpStatus.BAD_REQUEST;

		return new ResponseEntity<>(new ErrorMessage(httpStatus.toString(), customerException.getMessage()),
				httpStatus);
	}
}
