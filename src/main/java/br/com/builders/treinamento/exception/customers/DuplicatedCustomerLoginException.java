package br.com.builders.treinamento.exception.customers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class DuplicatedCustomerLoginException extends CustomerException {

	private static final long serialVersionUID = 1L;

	public DuplicatedCustomerLoginException(String message) {
		super(message);
	}
}
