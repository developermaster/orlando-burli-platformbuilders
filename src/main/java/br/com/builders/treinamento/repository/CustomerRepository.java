package br.com.builders.treinamento.repository;

import java.util.Optional;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.builders.treinamento.domain.Customer;

@Lazy
@Repository
public interface CustomerRepository extends MongoRepository<Customer, String> {

	/**
	 * Search a customer by a login different for the informed id.
	 *
	 * @param login
	 *            Login to find
	 * @param id
	 *            Id to exclude from search
	 * @return Optional Customer
	 */
	Optional<Customer> findByLoginAndIdNot(String login, String id);

	/**
	 * Search a customer by login
	 *
	 * @param login
	 *            Login to find
	 * @return Optional Customer
	 */
	Optional<Customer> findByLogin(String login);

	/**
	 * Search a customer by an id.
	 *
	 * @param id
	 * @return
	 */
	Optional<Customer> findById(final String id);

}
