package br.com.builders.treinamento.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.CustomerDto;
import br.com.builders.treinamento.exception.customers.CustomerException;
import br.com.builders.treinamento.exception.customers.CustomerNotFoundException;
import br.com.builders.treinamento.exception.customers.DuplicatedCustomerIdException;
import br.com.builders.treinamento.exception.customers.DuplicatedCustomerLoginException;
import br.com.builders.treinamento.exception.customers.InvalidCustomerDataException;
import br.com.builders.treinamento.repository.CustomerRepository;

@Lazy
@Service
public class CustomerService {

	private static final String CUSTOMER_CRM_ID_MANDATORY = "customer.crmId.mandatory";
	private static final String CUSTOMER_LOGIN_MANDATORY = "customer.login.mandatory";
	private static final String CUSTOMER_ID_MANDATORY = "customer.id.mandatory";
	private static final String CUSTOMER_NAME_MANDATORY = "customer.name.mandatory";
	private static final String CUSTOMER_ALREADY_EXISTS_LOGIN = "customer.already.exists.login";
	private static final String CUSTOMER_ALREADY_EXISTS_ID = "customer.already.exists.id";
	private static final String CUSTOMER_NOT_FOUND_ID = "customer.not.found.id";

	@Autowired
	private final CustomerRepository repository;

	@Autowired
	private final ModelMapper modelMapper;

	@Autowired
	private final MessageSource messageSource;

	@Autowired
	public CustomerService(CustomerRepository repository, ModelMapper modelMapper, MessageSource messageSource) {
		this.repository = repository;
		this.modelMapper = modelMapper;
		this.messageSource = messageSource;
	}

	public List<CustomerDto> listAll() {
		final List<Customer> findAll = this.repository.findAll();

		// Usa o linked list para manter a ordenação.
		final List<CustomerDto> lista = new LinkedList<>();

		findAll.forEach(v -> lista.add(this.modelMapper.map(v, CustomerDto.class)));

		return lista;
	}

	/**
	 * Insert a customer.
	 *
	 * @param customer
	 *            Customer to insert.
	 * @throws CustomerException
	 *             Throw a exception in case of any validation problems.
	 */
	public CustomerDto insert(CustomerDto customerDto) throws CustomerException {
		final Customer customer = this.modelMapper.map(customerDto, Customer.class);

		this.validateDuplicatedId(customer);

		this.validate(customer);

		final Customer insert = this.repository.insert(customer);

		return this.modelMapper.map(insert, CustomerDto.class);
	}

	/**
	 * Update a customer.
	 *
	 * @param customer
	 *            Customer to update.
	 * @throws CustomerException
	 *             Throw a exception in case of any validation problems.
	 */
	public CustomerDto update(String customerId, CustomerDto customerDto) throws CustomerException {
		final Optional<Customer> findById = this.repository.findById(customerId);

		if (findById.isPresent()) {
			final Customer customer = findById.get();

			BeanUtils.copyProperties(customerDto, customer, "id");

			this.validate(customer);

			final Customer updated = this.repository.save(customer);

			return this.modelMapper.map(updated, CustomerDto.class);
		} else {
			throw new CustomerNotFoundException(this.messageSource.getMessage(CUSTOMER_NOT_FOUND_ID,
					new Object[] { customerId }, Locale.getDefault()));
		}
	}

	/**
	 * Delete a customer
	 *
	 * @param customerId
	 *            Id of the customer to be deleted
	 * @throws CustomerNotFoundException
	 *             Throws if the customer cannot be found with the informed id.
	 */
	public void delete(String customerId) throws CustomerNotFoundException {
		final Optional<Customer> findById = this.repository.findById(customerId);

		if (findById.isPresent()) {
			this.repository.delete(findById.get());
		} else {
			throw new CustomerNotFoundException(this.messageSource.getMessage(CUSTOMER_NOT_FOUND_ID,
					new Object[] { customerId }, Locale.getDefault()));
		}
	}

	/**
	 * Partially update a customer.
	 *
	 * @param customerId
	 *            Id of the customer.
	 * @param customerDto
	 *            Data of the customer.
	 * @return Customer updated.
	 * @throws CustomerException
	 *             Throws an exception if any data was invalid, or if the customer
	 *             was not found.
	 */
	public CustomerDto patch(String customerId, CustomerDto customerDto) throws CustomerException {
		final Optional<Customer> findById = this.repository.findById(customerId);

		if (findById.isPresent()) {
			final Customer customer = findById.get();

			if (!StringUtils.isEmpty(customerDto.getName())) {
				customer.setName(customerDto.getName());
			}

			if (!StringUtils.isEmpty(customerDto.getCrmId())) {
				customer.setCrmId(customerDto.getCrmId());
			}

			if (!StringUtils.isEmpty(customerDto.getBaseUrl())) {
				customer.setBaseUrl(customerDto.getBaseUrl());
			}

			if (!StringUtils.isEmpty(customerDto.getLogin())) {
				customer.setLogin(customerDto.getLogin());
			}

			this.validate(customer);

			final Customer updated = this.repository.save(customer);

			return this.modelMapper.map(updated, CustomerDto.class);
		} else {
			throw new CustomerNotFoundException(this.messageSource.getMessage(CUSTOMER_NOT_FOUND_ID,
					new Object[] { customerId }, Locale.getDefault()));
		}
	}

	/**
	 * Returns a customer from the id.
	 *
	 * @param customerId
	 *            Id of the customer.
	 * @return Data of the customer.
	 * @throws CustomerNotFoundException
	 *             Throws if then Customer was not found with the informed id.
	 */
	public CustomerDto get(String customerId) throws CustomerNotFoundException {
		final Optional<Customer> findById = this.repository.findById(customerId);

		if (findById.isPresent()) {
			return this.modelMapper.map(findById.get(), CustomerDto.class);
		} else {
			throw new CustomerNotFoundException(this.messageSource.getMessage(CUSTOMER_NOT_FOUND_ID,
					new Object[] { customerId }, Locale.getDefault()));
		}
	}

	/**
	 * Several validations of a customer.
	 *
	 * @param customer
	 *            Customer to validate.
	 * @throws CustomerException
	 *             Throw a exception in case of any validation problems.
	 */
	private void validate(Customer customer) throws CustomerException {
		this.validateCustomerData(customer);
		this.validateDuplicatedLogin(customer);
	}

	/**
	 *
	 * @param customer
	 * @throws DuplicatedCustomerIdException
	 */
	private void validateDuplicatedId(Customer customer) throws DuplicatedCustomerIdException {
		final Optional<Customer> findById = this.repository.findById(customer.getId());

		if (findById.isPresent()) {
			throw new DuplicatedCustomerIdException(this.messageSource.getMessage(CUSTOMER_ALREADY_EXISTS_ID,
					new Object[] { customer.getId() }, Locale.getDefault()));
		}
	}

	/**
	 * Validate if there is another customer with the login informed in this
	 * customer.
	 *
	 * @param customer
	 *            Customer to validate.
	 * @throws DuplicatedCustomerLoginException
	 *             Throws if there is another customer with this login.
	 */
	private void validateDuplicatedLogin(Customer customer) throws DuplicatedCustomerLoginException {
		final Optional<Customer> findByLogin = this.repository.findByLoginAndIdNot(customer.getLogin(),
				customer.getId());

		if (findByLogin.isPresent()) {
			throw new DuplicatedCustomerLoginException(this.messageSource.getMessage(CUSTOMER_ALREADY_EXISTS_LOGIN,
					new Object[] { customer.getLogin() }, Locale.getDefault()));
		}
	}

	/**
	 * Validate the data for the customer.
	 *
	 * @param customer
	 *            Customer to validate.
	 * @throws InvalidCustomerDataException
	 *             Throws if there is any data missing or invalid.
	 */
	private void validateCustomerData(Customer customer) throws InvalidCustomerDataException {
		if (StringUtils.isEmpty(customer.getName())) {
			throw new InvalidCustomerDataException(
					this.messageSource.getMessage(CUSTOMER_NAME_MANDATORY, null, Locale.getDefault()));
		}

		if (StringUtils.isEmpty(customer.getId())) {
			throw new InvalidCustomerDataException(
					this.messageSource.getMessage(CUSTOMER_ID_MANDATORY, null, Locale.getDefault()));
		}

		if (StringUtils.isEmpty(customer.getLogin())) {
			throw new InvalidCustomerDataException(
					this.messageSource.getMessage(CUSTOMER_LOGIN_MANDATORY, null, Locale.getDefault()));
		}

		if (StringUtils.isEmpty(customer.getCrmId())) {
			throw new InvalidCustomerDataException(
					this.messageSource.getMessage(CUSTOMER_CRM_ID_MANDATORY, null, Locale.getDefault()));
		}
	}

}
