package br.com.builders.treinamento.builders;

import br.com.builders.treinamento.dto.CustomerDto;

public class CustomerBuilder {

	private final CustomerDto customer;

	public CustomerBuilder() {

		this.customer = new CustomerDto();
	}

	public static CustomerBuilder create() {

		return new CustomerBuilder();
	}

	public CustomerBuilder withName(String name) {

		this.customer.setName(name);

		return this;
	}

	public CustomerBuilder withLogin(String login) {

		this.customer.setLogin(login);

		return this;
	}

	public CustomerBuilder withCrm(String crmId) {

		this.customer.setCrmId(crmId);

		return this;
	}

	public CustomerBuilder withBaseUrl(String baseUrl) {

		this.customer.setBaseUrl(baseUrl);

		return this;
	}

	public CustomerBuilder withId(String customerId) {

		this.customer.setId(customerId);

		return this;
	}

	public CustomerDto build() {

		return this.customer;
	}

}
