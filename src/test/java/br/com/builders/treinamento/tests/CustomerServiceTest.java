package br.com.builders.treinamento.tests;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.javafaker.Faker;
import com.mongodb.BasicDBObject;

import br.com.builders.treinamento.builders.CustomerBuilder;
import br.com.builders.treinamento.dto.CustomerDto;
import br.com.builders.treinamento.exception.customers.CustomerException;
import br.com.builders.treinamento.exception.customers.CustomerNotFoundException;
import br.com.builders.treinamento.exception.customers.DuplicatedCustomerIdException;
import br.com.builders.treinamento.exception.customers.DuplicatedCustomerLoginException;
import br.com.builders.treinamento.exception.customers.InvalidCustomerDataException;
import br.com.builders.treinamento.service.CustomerService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@EnableAutoConfiguration
public class CustomerServiceTest {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private MongoTemplate mongoTemplate;

	@Rule
	public final ExpectedException thrown = ExpectedException.none();

	private final Faker facker = new Faker(Locale.ENGLISH);

	@Test
	public void insertMustPass() throws CustomerException {
		final CustomerDto joseph = this.buildCustomerJoseph();

		final CustomerDto inserted = this.customerService.insert(joseph);

		assertEquals(joseph.getId(), inserted.getId());
		assertEquals(joseph.getName(), inserted.getName());
		assertEquals(joseph.getBaseUrl(), inserted.getBaseUrl());
		assertEquals(joseph.getCrmId(), inserted.getCrmId());
		assertEquals(joseph.getLogin(), inserted.getLogin());
	}

	@Test
	public void insertMustFailNoName() throws CustomerException {
		this.thrown.expectMessage("Customer name was not informed.");
		this.thrown.expect(InvalidCustomerDataException.class);

		final CustomerDto joseph = this.buildCustomerJoseph();

		joseph.setName(null);

		this.customerService.insert(joseph);

		Assert.fail();
	}

	@Test
	public void insertMustFailNoId() throws CustomerException {
		this.thrown.expectMessage("Customer id was not informed.");
		this.thrown.expect(InvalidCustomerDataException.class);

		final CustomerDto joseph = this.buildCustomerJoseph();

		joseph.setId(null);

		this.customerService.insert(joseph);

		Assert.fail();
	}

	@Test
	public void insertMustFailNoLogin() throws CustomerException {
		this.thrown.expectMessage("Customer login was not informed.");
		this.thrown.expect(InvalidCustomerDataException.class);

		final CustomerDto joseph = this.buildCustomerJoseph();

		joseph.setLogin(null);

		this.customerService.insert(joseph);

		Assert.fail();
	}

	@Test
	public void insertMustFailNoCrm() throws CustomerException {
		this.thrown.expectMessage("Customer CRM was not informed.");
		this.thrown.expect(InvalidCustomerDataException.class);

		final CustomerDto joseph = this.buildCustomerJoseph();

		joseph.setCrmId(null);

		this.customerService.insert(joseph);

		Assert.fail();
	}

	@Test
	public void insertMustFailDuplicatedId() throws CustomerException {
		final CustomerDto joseph = this.buildCustomerJoseph();

		this.thrown.expect(DuplicatedCustomerIdException.class);
		this.thrown.expectMessage("Customer already exists with the id " + joseph.getId() + ".");

		this.customerService.insert(joseph);
		this.customerService.insert(joseph);

		Assert.fail();
	}

	@Test
	public void insertMustFailDuplicatedLogin() throws CustomerException {
		final CustomerDto joseph = this.buildCustomerJoseph();
		final CustomerDto mirian = this.buildCustomerMirian();

		// Duplicate the login for both users.
		mirian.setLogin("logintoduplicate@gmail.com");
		joseph.setLogin("logintoduplicate@gmail.com");

		this.thrown.expect(DuplicatedCustomerLoginException.class);
		this.thrown.expectMessage("Customer already exists with the login " + joseph.getLogin() + ".");

		this.customerService.insert(joseph);
		this.customerService.insert(mirian);

		Assert.fail();
	}

	@Test
	public void updateMustPass() throws CustomerException {
		final CustomerDto joseph = this.buildCustomerJoseph();

		// First of all, must insert the joseph.
		this.customerService.insert(joseph);

		// Then, we will test the update.
		final CustomerDto updated = this.customerService.update(joseph.getId(), joseph);

		assertEquals(joseph.getId(), updated.getId());
		assertEquals(joseph.getName(), updated.getName());
		assertEquals(joseph.getBaseUrl(), updated.getBaseUrl());
		assertEquals(joseph.getCrmId(), updated.getCrmId());
		assertEquals(joseph.getLogin(), updated.getLogin());
	}

	@Test
	public void updateMustFailCustomerNotFound() throws CustomerException {
		final CustomerDto joseph = this.buildCustomerJoseph();

		this.thrown.expect(CustomerNotFoundException.class);
		this.thrown.expectMessage("Customer not found with the id " + joseph.getId() + ".");

		this.customerService.update(joseph.getId(), joseph);
	}

	@Test
	public void patchChangeNameMustPass() throws CustomerException {
		final CustomerDto joseph = this.buildCustomerJoseph();

		// First, insert the joseph user.
		this.customerService.insert(joseph);

		// Then, try to patch data.
		final CustomerDto patchCustomerJoseph = CustomerBuilder.create().withName("Joseph Miller").build();

		final CustomerDto patched = this.customerService.patch(joseph.getId(), patchCustomerJoseph);

		assertEquals(joseph.getId(), patched.getId());
		assertEquals(patchCustomerJoseph.getName(), patched.getName());
		assertEquals(joseph.getBaseUrl(), patched.getBaseUrl());
		assertEquals(joseph.getCrmId(), patched.getCrmId());
		assertEquals(joseph.getLogin(), patched.getLogin());
	}

	@Test
	public void patchChangeCrmMustPass() throws CustomerException {
		final CustomerDto joseph = this.buildCustomerJoseph();

		// First, insert the joseph user.
		this.customerService.insert(joseph);

		// Then, try to patch data.
		final CustomerDto patchCustomerJoseph = CustomerBuilder.create().withCrm("CRM-999").build();

		final CustomerDto patched = this.customerService.patch(joseph.getId(), patchCustomerJoseph);

		assertEquals(joseph.getId(), patched.getId());
		assertEquals(joseph.getName(), patched.getName());
		assertEquals(joseph.getBaseUrl(), patched.getBaseUrl());
		assertEquals(patchCustomerJoseph.getCrmId(), patched.getCrmId());
		assertEquals(joseph.getLogin(), patched.getLogin());
	}

	@Test
	public void patchChangeBaseUrlMustPass() throws CustomerException {
		final CustomerDto joseph = this.buildCustomerJoseph();

		// First, insert the joseph user.
		this.customerService.insert(joseph);

		// Then, try to patch data.
		final CustomerDto patchCustomerJoseph = CustomerBuilder.create().withBaseUrl("http://www.universal.com")
				.build();

		final CustomerDto patched = this.customerService.patch(joseph.getId(), patchCustomerJoseph);

		assertEquals(joseph.getId(), patched.getId());
		assertEquals(joseph.getName(), patched.getName());
		assertEquals(patchCustomerJoseph.getBaseUrl(), patched.getBaseUrl());
		assertEquals(joseph.getCrmId(), patched.getCrmId());
		assertEquals(joseph.getLogin(), patched.getLogin());
	}

	@Test
	public void patchChangeLoginMustPass() throws CustomerException {
		final CustomerDto joseph = this.buildCustomerJoseph();

		// First, insert the joseph user.
		this.customerService.insert(joseph);

		// Then, try to patch data.
		final CustomerDto patchCustomerJoseph = CustomerBuilder.create().withLogin("newlogin@gmail.com").build();

		final CustomerDto patched = this.customerService.patch(joseph.getId(), patchCustomerJoseph);

		assertEquals(joseph.getId(), patched.getId());
		assertEquals(joseph.getName(), patched.getName());
		assertEquals(joseph.getBaseUrl(), patched.getBaseUrl());
		assertEquals(joseph.getCrmId(), patched.getCrmId());
		assertEquals(patchCustomerJoseph.getLogin(), patched.getLogin());
	}

	@Test
	public void patchChangeLoginMustFailDuplicatedLogin() throws CustomerException {
		final CustomerDto joseph = this.buildCustomerJoseph();
		final CustomerDto mirian = this.buildCustomerMirian();

		// First, insert the joseph and mirian user.
		this.customerService.insert(joseph);
		this.customerService.insert(mirian);

		// This exception must throw, with the specified message.
		this.thrown.expect(DuplicatedCustomerLoginException.class);
		this.thrown.expectMessage("Customer already exists with the login " + mirian.getLogin() + ".");

		// Then, try to patch data, changing joseph login to use same mirian login.
		final CustomerDto patchCustomerJoseph = CustomerBuilder.create().withLogin(mirian.getLogin()).build();

		this.customerService.patch(joseph.getId(), patchCustomerJoseph);

		Assert.fail();
	}

	@Test
	public void patchMustFailIdNotFound() throws CustomerException {
		final CustomerDto mirian = this.buildCustomerMirian();

		// This exception must throw, with the specified message.
		this.thrown.expect(CustomerNotFoundException.class);
		this.thrown.expectMessage("Customer not found with the id " + mirian.getId() + ".");

		// Then, try to patch data, changing mirian login, but she is not persisted.
		final CustomerDto pacthCustomerMirian = CustomerBuilder.create().withLogin(mirian.getLogin()).build();

		this.customerService.patch(mirian.getId(), pacthCustomerMirian);

		Assert.fail();
	}

	@Test
	public void deleteMustPass() throws CustomerException {
		final CustomerDto mirian = this.buildCustomerMirian();

		// First, insert data.
		this.customerService.insert(mirian);

		// Then try delete it.
		this.customerService.delete(mirian.getId());
	}

	@Test
	public void deleteMusFailCustomerNotFound() throws CustomerNotFoundException {
		final String customerId = "XXX";

		// This exception must throw, with the specified message.
		this.thrown.expect(CustomerNotFoundException.class);
		this.thrown.expectMessage("Customer not found with the id " + customerId + ".");

		this.customerService.delete(customerId);
	}

	@Test
	public void getMustPass() throws CustomerException {
		final CustomerDto joseph = this.buildCustomerJoseph();

		// First of all, must insert the joseph.
		this.customerService.insert(joseph);

		final CustomerDto get = this.customerService.get(joseph.getId());

		assertEquals(joseph.getId(), get.getId());
		assertEquals(joseph.getName(), get.getName());
		assertEquals(joseph.getBaseUrl(), get.getBaseUrl());
		assertEquals(joseph.getCrmId(), get.getCrmId());
		assertEquals(joseph.getLogin(), get.getLogin());
	}

	@Test
	public void getMustFailCustomerNotFound() throws CustomerNotFoundException {
		final String customerId = "XXX-123123-MASD4";

		this.thrown.expect(CustomerNotFoundException.class);
		this.thrown.expectMessage("Customer not found with the id " + customerId + ".");

		this.customerService.get(customerId);
	}

	@Test
	public void getMustFailNoId() throws CustomerNotFoundException {
		this.thrown.expect(CustomerNotFoundException.class);
		this.thrown.expectMessage("Customer not found with the id null.");

		this.customerService.get(null);
	}

	@Test
	public void listAllMustPass() throws CustomerException {
		final int TAMANHO_LISTA = 50;

		for (int i = 0; i < TAMANHO_LISTA; i++) {
			this.customerService.insert(this.buildRandomCustomer());
		}

		final List<CustomerDto> listAll = this.customerService.listAll();

		assertEquals(TAMANHO_LISTA, listAll.size());
	}

	@Before
	public void prepare() {
		// Clear the data before each test.
		for (final String collectionName : this.mongoTemplate.getCollectionNames()) {
			if (!collectionName.startsWith("system.")) {
				this.mongoTemplate.getCollection(collectionName).remove(new BasicDBObject());
			}
		}
	}

	public CustomerDto buildCustomerJoseph() {
		// @formatter:off
		return CustomerBuilder.create()
				.withId("xxx-123-90xxs")
				.withName("Joseph Dalton")
				.withCrm("C12338")
				.withBaseUrl("http://www.terra.com.br")
				.withLogin("joseph@gmail.com")
				.build();
		// @formatter:on
	}

	public CustomerDto buildCustomerMirian() {
		// @formatter:off
		return CustomerBuilder.create()
				.withId("AAS-123i8-YU87")
				.withName("Mirian Clark")
				.withCrm("C7655")
				.withBaseUrl("http://www.mirian.com")
				.withLogin("mirian.clark@terra.com.br")
				.build();
		// @formatter:on
	}

	public CustomerDto buildRandomCustomer() {
		// @formatter:off
		final String fullName = this.facker.name().fullName();
		return CustomerBuilder.create()
				.withId(this.facker.idNumber().ssnValid())
				.withName(fullName)
				.withCrm(this.facker.idNumber().valid())
				.withBaseUrl("http://wwww."+ this.facker.university().name().replaceAll(" ", "").toLowerCase() + ".com")
				.withLogin(fullName.replaceAll(" ", ".").toLowerCase() + "@gmail.com")
				.build();
		// @formatter:on
	}

}
