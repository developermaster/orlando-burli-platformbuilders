package br.com.builders.treinamento.tests;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;

import br.com.builders.treinamento.builders.CustomerBuilder;
import br.com.builders.treinamento.dto.CustomerDto;
import br.com.builders.treinamento.exception.ErrorMessage;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@EnableAutoConfiguration
public class CustomerE2ETest {

	private static final String API_CUSTOMERS = "http://localhost:8080/api/customers";

	@Autowired
	private MongoTemplate mongoTemplate;

	private Faker facker;

	@Rule
	public final ExpectedException thrown = ExpectedException.none();

	@Test
	public void insertMustPass() throws Exception {
		final CustomerDto joseph = this.buildCustomerJoseph();

		final RestTemplate template = new RestTemplate();
		final ResponseEntity<String> response = template.postForEntity(API_CUSTOMERS, joseph, String.class);

		assertEquals(201, response.getStatusCodeValue());
		assertEquals(API_CUSTOMERS + "/" + joseph.getId(), response.getHeaders().get("location").get(0));
	}

	@Test
	public void insertMustFailNoName() {
		this.thrown.expect(HttpClientErrorException.class);

		final CustomerDto joseph = this.buildCustomerJoseph();
		joseph.setName(null);

		final RestTemplate template = new RestTemplate();
		final ResponseEntity<String> response = template.postForEntity(API_CUSTOMERS, joseph, String.class);

		System.out.println(response.getBody());

		assertEquals(400, response.getStatusCodeValue());

		final ErrorMessage errorMessage = new Gson().fromJson(response.getBody(), ErrorMessage.class);
		assertEquals("Customer name was not informed.", errorMessage.getMessage());
		assertEquals("400", errorMessage.getCode());
	}

	@Test
	public void getMustFailCustomerNotFound() throws Exception {
		this.thrown.expect(HttpClientErrorException.class);

		final String customerId = "XXX-123123-MASD4";

		final RestTemplate template = new RestTemplate();
		final ResponseEntity<String> response = template.getForEntity(API_CUSTOMERS + "/" + customerId, String.class);

		assertEquals(404, response.getStatusCodeValue());

		final ErrorMessage errorMessage = new Gson().fromJson(response.getBody(), ErrorMessage.class);

		assertEquals("Customer not found with the id " + customerId + ".", errorMessage.getMessage());
	}

	public CustomerDto buildCustomerJoseph() {
		// @formatter:off
		return CustomerBuilder.create()
				.withId("xxx-123-90xxs")
				.withName("Joseph Dalton")
				.withCrm("C12338")
				.withBaseUrl("http://www.terra.com.br")
				.withLogin("joseph@gmail.com")
				.build();
		// @formatter:on
	}

	public CustomerDto buildCustomerMirian() {
		// @formatter:off
		return CustomerBuilder.create()
				.withId("AAS-123i8-YU87")
				.withName("Mirian Clark")
				.withCrm("C7655")
				.withBaseUrl("http://www.mirian.com")
				.withLogin("mirian.clark@terra.com.br")
				.build();
		// @formatter:on
	}

	public CustomerDto buildRandomCustomer() {
		// @formatter:off
		final String fullName = this.facker.name().fullName();
		return CustomerBuilder.create()
				.withId(this.facker.idNumber().ssnValid())
				.withName(fullName)
				.withCrm(this.facker.idNumber().valid())
				.withBaseUrl("http://wwww."+ this.facker.university().name().replaceAll(" ", "").toLowerCase() + ".com")
				.withLogin(fullName.replaceAll(" ", ".").toLowerCase() + "@gmail.com")
				.build();
		// @formatter:on
	}

	@Before
	public void prepare() {
		this.facker = new Faker(Locale.ENGLISH);

		// Clear the data before each test.
		for (final String collectionName : this.mongoTemplate.getCollectionNames()) {
			if (!collectionName.startsWith("system.")) {
				this.mongoTemplate.getCollection(collectionName).remove(new BasicDBObject());
			}
		}
	}
}
