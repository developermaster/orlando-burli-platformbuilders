package br.com.builders.treinamento.tests;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.mongodb.BasicDBObject;

import br.com.builders.treinamento.builders.CustomerBuilder;
import br.com.builders.treinamento.dto.CustomerDto;
import br.com.builders.treinamento.resources.CustomerResource;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@EnableAutoConfiguration
public class CustomerResourceTest {

	private static final int MAXIMO = 30;

	@Autowired
	private CustomerResource customerResource;

	@Autowired
	private MongoTemplate mongoTemplate;

	private ObjectMapper mapper;

	private MockMvc mockMvc;

	private Faker facker;

	@Test
	public void insertMustPass() throws Exception {
		// @formatter:off
		this.mockMvc
				.perform(post("/customers")
						.contentType(MediaType.APPLICATION_JSON_UTF8)
						.content(this.mapper.writeValueAsString(this.buildCustomerJoseph())))
				.andDo(print())
				.andExpect(status().isCreated());
		// @formatter:on
	}

	@Test
	public void insertMustFailNoName() throws Exception {
		final CustomerDto joseph = this.buildCustomerJoseph();
		joseph.setName(null);

		// @formatter:off
		final String message =
				this.mockMvc
					.perform(post("/customers")
							.contentType(MediaType.APPLICATION_JSON_UTF8)
							.content(this.mapper.writeValueAsString(joseph)))
					.andDo(print())
					.andExpect(status().isBadRequest())
					.andReturn()
						.getResolvedException()
						.getMessage();
		// @formatter:on
		assertEquals("Customer name was not informed.", message);
	}

	@Test
	public void insertMustFailNoId() throws Exception {
		final CustomerDto joseph = this.buildCustomerJoseph();
		joseph.setId(null);

		// @formatter:off
		final String message =
				this.mockMvc
					.perform(post("/customers")
							.contentType(MediaType.APPLICATION_JSON_UTF8)
							.content(this.mapper.writeValueAsString(joseph)))
					.andDo(print())
					.andExpect(status().isBadRequest())
					.andReturn()
						.getResolvedException()
						.getMessage();
		// @formatter:on
		assertEquals("Customer id was not informed.", message);
	}

	@Test
	public void insertMustFailNoLogin() throws Exception {
		final CustomerDto joseph = this.buildCustomerJoseph();
		joseph.setLogin(null);

		// @formatter:off
		final String message =
				this.mockMvc
					.perform(post("/customers")
							.contentType(MediaType.APPLICATION_JSON_UTF8)
							.content(this.mapper.writeValueAsString(joseph)))
					.andDo(print())
					.andExpect(status().isBadRequest())
					.andReturn()
						.getResolvedException()
						.getMessage();
		// @formatter:on
		assertEquals("Customer login was not informed.", message);
	}

	@Test
	public void insertMustFailNoCrm() throws Exception {
		final CustomerDto joseph = this.buildCustomerJoseph();
		joseph.setCrmId(null);

		// @formatter:off
		final String message =
				this.mockMvc
					.perform(post("/customers")
							.contentType(MediaType.APPLICATION_JSON_UTF8)
							.content(this.mapper.writeValueAsString(joseph)))
					.andDo(print())
					.andExpect(status().isBadRequest())
					.andReturn()
						.getResolvedException()
						.getMessage();
		// @formatter:on
		assertEquals("Customer CRM was not informed.", message);
	}

	@Test
	public void insertMustFailDuplicatedId() throws Exception {
		// @formatter:off
		final CustomerDto joseph = this.buildCustomerJoseph();

		this.mockMvc
				.perform(post("/customers")
						.contentType(MediaType.APPLICATION_JSON_UTF8)
						.content(this.mapper.writeValueAsString(joseph)))
				.andDo(print())
				.andExpect(status().isCreated());
		// @formatter:on

		// @formatter:off
		final String message =
				this.mockMvc
					.perform(post("/customers")
							.contentType(MediaType.APPLICATION_JSON_UTF8)
							.content(this.mapper.writeValueAsString(joseph)))
					.andDo(print())
					.andExpect(status().isConflict())
					.andReturn()
						.getResolvedException()
						.getMessage();
		// @formatter:on
		assertEquals("Customer already exists with the id " + joseph.getId() + ".", message);
	}

	@Test
	public void insertMustFailDuplicatedLogin() throws Exception {
		final CustomerDto joseph = this.buildCustomerJoseph();
		final CustomerDto mirian = this.buildCustomerMirian();

		// Duplicate the login for both users.
		mirian.setLogin("logintoduplicate@gmail.com");
		joseph.setLogin("logintoduplicate@gmail.com");

		// @formatter:off
		this.mockMvc
				.perform(post("/customers")
						.contentType(MediaType.APPLICATION_JSON_UTF8)
						.content(this.mapper.writeValueAsString(joseph)))
				.andDo(print())
				.andExpect(status().isCreated());
		// @formatter:on

		// @formatter:off
		final String message =
				this.mockMvc
					.perform(post("/customers")
							.contentType(MediaType.APPLICATION_JSON_UTF8)
							.content(this.mapper.writeValueAsString(mirian)))
					.andDo(print())
					.andExpect(status().isConflict())
					.andReturn()
						.getResolvedException()
						.getMessage();
		// @formatter:on
		assertEquals("Customer already exists with the login " + joseph.getLogin() + ".", message);
	}

	@Test
	public void updateMustPass() throws Exception {
		final CustomerDto joseph = this.buildCustomerJoseph();

		// Insert data for later tests.
		this.insertData(joseph);

		// @formatter:off
		this.mockMvc
				.perform(put("/customers/" + joseph.getId())
						.contentType(MediaType.APPLICATION_JSON_UTF8)
						.content(this.mapper.writeValueAsString(joseph)))
				.andDo(print())
				.andExpect(status().isOk());
		// @formatter:on
	}

	@Test
	public void updateMustFailCustomerNotFound() throws Exception {
		final CustomerDto joseph = this.buildCustomerJoseph();

		// @formatter:off
		final String message = this.mockMvc
				.perform(put("/customers/" + joseph.getId())
						.contentType(MediaType.APPLICATION_JSON_UTF8)
						.content(this.mapper.writeValueAsString(joseph)))
				.andDo(print())
				.andExpect(status().isNotFound())
				.andReturn()
					.getResolvedException()
					.getMessage();
		// @formatter:on
		assertEquals("Customer not found with the id " + joseph.getId() + ".", message);
	}

	@Test
	public void patchChangeNameMustPass() throws Exception {
		final CustomerDto joseph = this.buildCustomerJoseph();

		// First insert the data
		this.insertData(joseph);

		// Prepare new data only with the name to partial update
		final CustomerDto josephPatched = CustomerBuilder.create().withName("Joseph Miller").build();

		// @formatter:off
		this.mockMvc
				.perform(patch("/customers/" + joseph.getId())
						.contentType(MediaType.APPLICATION_JSON_UTF8)
						.content(this.mapper.writeValueAsString(josephPatched)))
				.andDo(print())
				.andExpect(status().isOk());
		// @formatter:on

		// Get's the new joseph data to compare
		// @formatter:off
		this.mockMvc
				.perform(get("/customers/" + joseph.getId())
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is(josephPatched.getName())))
				.andExpect(jsonPath("$.id", is(joseph.getId())))
				.andExpect(jsonPath("$.crmId", is(joseph.getCrmId())))
				.andExpect(jsonPath("$.login", is(joseph.getLogin())))
				.andExpect(jsonPath("$.baseUrl", is(joseph.getBaseUrl())));
		// @formatter:on
	}

	@Test
	public void patchChangeCrmMustPass() throws Exception {
		final CustomerDto joseph = this.buildCustomerJoseph();

		// First insert the data
		this.insertData(joseph);

		// Prepare new data only with the crmId to partial update
		final CustomerDto josephPatched = CustomerBuilder.create().withCrm("CRM-999").build();

		// @formatter:off
		this.mockMvc
				.perform(patch("/customers/" + joseph.getId())
						.contentType(MediaType.APPLICATION_JSON_UTF8)
						.content(this.mapper.writeValueAsString(josephPatched)))
				.andDo(print())
				.andExpect(status().isOk());
		// @formatter:on

		// Get's the new joseph data to compare
		// @formatter:off
		this.mockMvc
				.perform(get("/customers/" + joseph.getId())
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is(joseph.getName())))
				.andExpect(jsonPath("$.id", is(joseph.getId())))
				.andExpect(jsonPath("$.crmId", is(josephPatched.getCrmId())))
				.andExpect(jsonPath("$.login", is(joseph.getLogin())))
				.andExpect(jsonPath("$.baseUrl", is(joseph.getBaseUrl())));
		// @formatter:on
	}

	@Test
	public void patchChangeBaseUrlMustPass() throws Exception {
		final CustomerDto joseph = this.buildCustomerJoseph();

		// First insert the data
		this.insertData(joseph);

		// Prepare new data only with the crmId to partial update
		final CustomerDto josephPatched = CustomerBuilder.create().withBaseUrl("http://www.universal.com").build();

		// @formatter:off
		this.mockMvc
				.perform(patch("/customers/" + joseph.getId())
						.contentType(MediaType.APPLICATION_JSON_UTF8)
						.content(this.mapper.writeValueAsString(josephPatched)))
				.andDo(print())
				.andExpect(status().isOk());
		// @formatter:on

		// Get's the new joseph data to compare
		// @formatter:off
		this.mockMvc
				.perform(get("/customers/" + joseph.getId())
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is(joseph.getName())))
				.andExpect(jsonPath("$.id", is(joseph.getId())))
				.andExpect(jsonPath("$.crmId", is(joseph.getCrmId())))
				.andExpect(jsonPath("$.login", is(joseph.getLogin())))
				.andExpect(jsonPath("$.baseUrl", is(josephPatched.getBaseUrl())));
		// @formatter:on
	}

	@Test
	public void patchChangeLoginMustPass() throws Exception {
		final CustomerDto joseph = this.buildCustomerJoseph();

		// First insert the data
		this.insertData(joseph);

		// Prepare new data only with the crmId to partial update
		final CustomerDto josephPatched = CustomerBuilder.create().withLogin("newlogin@gmail.com").build();

		// @formatter:off
		this.mockMvc
				.perform(patch("/customers/" + joseph.getId())
						.contentType(MediaType.APPLICATION_JSON_UTF8)
						.content(this.mapper.writeValueAsString(josephPatched)))
				.andDo(print())
				.andExpect(status().isOk());
		// @formatter:on

		// Get's the new joseph data to compare
		// @formatter:off
		this.mockMvc
				.perform(get("/customers/" + joseph.getId())
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is(joseph.getName())))
				.andExpect(jsonPath("$.id", is(joseph.getId())))
				.andExpect(jsonPath("$.crmId", is(joseph.getCrmId())))
				.andExpect(jsonPath("$.login", is(josephPatched.getLogin())))
				.andExpect(jsonPath("$.baseUrl", is(joseph.getBaseUrl())));
		// @formatter:on
	}

	@Test
	public void patchChangeLoginMustFailDuplicatedLogin() throws Exception {
		final CustomerDto joseph = this.buildCustomerJoseph();
		final CustomerDto mirian = this.buildCustomerMirian();

		// First, insert the joseph and mirian user.
		this.insertData(joseph);
		this.insertData(mirian);

		// Then, try to patch data, changing joseph login to use same mirian login.
		final CustomerDto josephPatched = CustomerBuilder.create().withLogin(mirian.getLogin()).build();

		// Get's the new joseph data to compare
		// @formatter:off
		final String message =
				this.mockMvc
					.perform(patch("/customers/"+ joseph.getId())
							.contentType(MediaType.APPLICATION_JSON_UTF8)
							.content(this.mapper.writeValueAsString(josephPatched)))
					.andDo(print())
					.andExpect(status().isConflict())
					.andReturn()
						.getResolvedException()
						.getMessage();
		// @formatter:on
		assertEquals("Customer already exists with the login " + mirian.getLogin() + ".", message);
	}

	@Test
	public void patchMustFailIdNotFound() throws Exception {
		final CustomerDto mirian = this.buildCustomerMirian();

		// @formatter:off
		final String message =
				this.mockMvc
					.perform(patch("/customers/"+ mirian.getId())
							.contentType(MediaType.APPLICATION_JSON_UTF8)
							.content(this.mapper.writeValueAsString(mirian)))
					.andDo(print())
					.andExpect(status().isNotFound())
					.andReturn()
						.getResolvedException()
						.getMessage();
		// @formatter:on
		assertEquals("Customer not found with the id " + mirian.getId() + ".", message);
	}

	@Test
	public void deleteMustPass() throws Exception {
		final CustomerDto mirian = this.buildCustomerMirian();

		// // First, insert data.
		this.insertData(mirian);

		// Then try delete it.

		// @formatter:off
		this.mockMvc
			.perform(delete("/customers/"+ mirian.getId())
					.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andDo(print())
			.andExpect(status().isOk());
		// @formatter:on

	}

	@Test
	public void deleteMusFailCustomerNotFound() throws Exception {
		final String customerId = "XXX";

		// This exception must throw, with the specified message.
		// @formatter:off
		this.mockMvc
			.perform(delete("/customers/"+ customerId)
					.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andDo(print())
			.andExpect(status().isNotFound())
			.andReturn()
				.getResolvedException()
				.getMessage();
		// @formatter:on
	}

	@Test
	public void getMustPass() throws Exception {
		final CustomerDto joseph = this.buildCustomerJoseph();

		// First of all, must insert the joseph.
		this.insertData(joseph);

		// @formatter:off
		this.mockMvc
				.perform(get("/customers/" + joseph.getId())
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name", is(joseph.getName())))
				.andExpect(jsonPath("$.id", is(joseph.getId())))
				.andExpect(jsonPath("$.crmId", is(joseph.getCrmId())))
				.andExpect(jsonPath("$.login", is(joseph.getLogin())))
				.andExpect(jsonPath("$.baseUrl", is(joseph.getBaseUrl())));
		// @formatter:on
	}

	@Test
	public void getMustFailCustomerNotFound() throws Exception {
		final String customerId = "XXX-123123-MASD4";

		// @formatter:off
		final String message = this.mockMvc
				.perform(get("/customers/" + customerId)
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isNotFound())
				.andReturn()
					.getResolvedException()
					.getMessage();
		// @formatter:on
		assertEquals("Customer not found with the id " + customerId + ".", message);
	}

	@Test
	public void getMustFailNoId() throws Exception {

		// @formatter:off
		final String message = this.mockMvc
				.perform(get("/customers/null")
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isNotFound())
				.andReturn()
					.getResolvedException()
					.getMessage();
		// @formatter:on
		assertEquals("Customer not found with the id null.", message);
	}

	@Test
	public void listAllMustPass() throws Exception {
		for (int i = 0; i < MAXIMO; i++) {
			this.insertData(this.buildRandomCustomer());
		}

		// @formatter:off
		this.mockMvc
				.perform(get("/customers")
							.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
				.andDo(print())
				.andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(MAXIMO)))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
		// @formatter:on
	}

	private void insertData(CustomerDto customer) throws Exception, JsonProcessingException {
		// @formatter:off
		this.mockMvc
				.perform(post("/customers")
						.contentType(MediaType.APPLICATION_JSON_UTF8)
						.content(this.mapper.writeValueAsString(customer)));
		// @formatter:on
	}

	public CustomerDto buildCustomerJoseph() {
		// @formatter:off
		return CustomerBuilder.create()
				.withId("xxx-123-90xxs")
				.withName("Joseph Dalton")
				.withCrm("C12338")
				.withBaseUrl("http://www.terra.com.br")
				.withLogin("joseph@gmail.com")
				.build();
		// @formatter:on
	}

	public CustomerDto buildCustomerMirian() {
		// @formatter:off
		return CustomerBuilder.create()
				.withId("AAS-123i8-YU87")
				.withName("Mirian Clark")
				.withCrm("C7655")
				.withBaseUrl("http://www.mirian.com")
				.withLogin("mirian.clark@terra.com.br")
				.build();
		// @formatter:on
	}

	public CustomerDto buildRandomCustomer() {
		// @formatter:off
		final String fullName = this.facker.name().fullName();
		return CustomerBuilder.create()
				.withId(this.facker.idNumber().ssnValid())
				.withName(fullName)
				.withCrm(this.facker.idNumber().valid())
				.withBaseUrl("http://wwww."+ this.facker.university().name().replaceAll(" ", "").toLowerCase() + ".com")
				.withLogin(fullName.replaceAll(" ", ".").toLowerCase() + "@gmail.com")
				.build();
		// @formatter:on
	}

	@Before
	public void prepare() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(this.customerResource).build();
		this.facker = new Faker(Locale.ENGLISH);
		this.mapper = new ObjectMapper();

		// Clear the data before each test.
		for (final String collectionName : this.mongoTemplate.getCollectionNames()) {
			if (!collectionName.startsWith("system.")) {
				this.mongoTemplate.getCollection(collectionName).remove(new BasicDBObject());
			}
		}
	}

}
